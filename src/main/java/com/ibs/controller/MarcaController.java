package com.ibs.controller;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;

import javax.validation.Valid;

import com.ibs.entities.Marcas;
import com.ibs.repository.MarcasRepository;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

/**
 * RolController
 */
@RestController
@RequestMapping(value = "/marca/")
@CrossOrigin
public class MarcaController {
    
	/** Instancia_Repositorio */
    @Autowired MarcasRepository repository;
    
    /** Metodo_listar*/
    @GetMapping(produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public Object getDatos() {
        String iconoEditar = "<i class='fas fa-edit'></i> <strong ></strong>";
        String iconoEliminar = "<i class='fas fa-trash-alt text-black'></i> <strong ></strong>";
        
        List<HashMap<String, Object>> registros = new ArrayList<>();
        List<Marcas> lista = (List<Marcas>) repository.findAll();
        
        for (Marcas entity : lista) {
            HashMap<String, Object> object = new HashMap<>();
            
            object.put("id", entity.getId());
            object.put("marca", entity.getMarca());
            object.put("operacion",
                    "<button type='button' data-toggle='modal' data-target='#editar' class='text-black btn btn-warning ml-3 mt-1'"
                            + "onclick='cargarRegistro(" + entity.getId() + ")'> " + iconoEditar + "</button>"
                            + "<button type='button' data-toggle='modal' data-target='#eliminar' style='color: black' class=' btn btn-danger ml-3 mt-1'"
                            + "onclick='cargarRegistro(" + entity.getId() + ")'> " + iconoEliminar + "</button>");

            registros.add(object);
        }
        return Collections.singletonMap("data", registros);
    }

    /** Metodo_para_editar o guardar_un_registro */
    /** Recibe_entitidad_desde_formulario */
    @PutMapping(produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public ResponseEntity<Marcas> save(@RequestBody @Valid Marcas entity) {
        return ResponseEntity.ok(repository.save(entity));
    }
    
    //Metodo_para buscar_registro por_ID y_cargarlo en_modal_editar
    @PostMapping(value="{id}", produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public Marcas getMethodName(@PathVariable Long id) {
        return repository.findById(id).get();
    }
    
    //Metodo_para_eliminar_registro
    @DeleteMapping(value="{id}", produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public Boolean delete(@PathVariable Long id) {
    	Marcas entity = repository.findById(id).get();
    	repository.delete(entity);
        return true;
    }
}