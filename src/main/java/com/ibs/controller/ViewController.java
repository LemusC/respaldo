package com.ibs.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.GetMapping;

import com.ibs.repository.CategoriasRepository;
import com.ibs.repository.MarcasRepository;
import com.ibs.repository.RoleRepository;
import com.ibs.services.VentaService;

@Controller
public class ViewController {

  RoleRepository rpRoles;
  VentaService sVenta;
  MarcasRepository rpMarca;
  CategoriasRepository rpCategoria;
  
  
  @Autowired
  public ViewController(RoleRepository rpRoles, VentaService sVenta, MarcasRepository rpMarca,
		CategoriasRepository rpCategoria) {
	this.rpRoles = rpRoles;
	this.sVenta = sVenta;
	this.rpMarca = rpMarca;
	this.rpCategoria = rpCategoria;
}

/** Redirige_vista_listar */
  /*@GetMapping(value = "roles")
  public String roles() {
    return "vistas/Rol/Listar";
  }*/

  /** Redirige_vista_listar */
  @GetMapping(value = "marcas")
  public String marcas() {
    return "vistas/Marcas/Listar";
  }

  /** Redirige_vista_listar */
  @GetMapping(value = "categorias")
  public String categorias() {
    return "vistas/Categorias/Listar";
  }

  /** Redirige_vista_listar */
  @GetMapping(value = "usuarios")
  public String user(ModelMap mp) {
    mp.put("list", rpRoles.findAll());
    return "vistas/User/Listar";
  }

  /** Redirige_vista_listar */
  @GetMapping(value = "proveedores")
  public String proveedores() {
    return "vistas/Proveedores/Listar";
  }

  /** Redirige_vista_listar */
  @GetMapping(value = "inventarios")
  public String inventario() {
    return "vistas/Inventario/Listar";
  }
  
  @GetMapping(value = "nuevoMovimiento")
  public String nuevoMovimiento() {
    return "vistas/Inventario/agregarMovimiento";                       
  }

  @GetMapping(value = "clientes")
  public String cliente() {
    return "vistas/Clientes/Listar";
  }

  /** Redirige_vista_listar */
  @GetMapping(value = "productos")
  public String productos(ModelMap mp) {
	    mp.put("list", rpMarca.findAll());
	    mp.put("listC", rpCategoria.findAll());
    return "vistas/Productos/Listar";
  }

  @GetMapping(value = "nuevaVenta")
  public String nuevaVenta(ModelMap mp) {
    mp.put("factura", sVenta.getNumeroFactura());
    return "vistas/Ventas/agregarVenta";
  }
  
  @GetMapping(value = "ventas")
  public String ventas() {
    return "vistas/Ventas/Listar";
  }

  @GetMapping(value = "nuevaCompra")
  public String nuevaCompra() {
    return "vistas/Compras/agregarCompra";
  }
  
  @GetMapping(value = "compras")
  public String compras() {
	  return "vistas/Compras/Listar";
  }
  
  @GetMapping(value = "listarCompra")
  public String ListarCompra() {
    return "vistas/Compras/ListarCompra";
  }
  
//redirige al listado de las ventas
  @GetMapping(value = "listarVenta")
  public String ListarVenta() {
    return "vistas/Ventas/ListarVenta";
  }
  
  @GetMapping(value = "inicio")
  public String inicio() {
    return "vistas/Inicio/index";
  }
}
