package com.ibs.controller;

import javax.transaction.Transactional;

import com.ibs.entities.Productos;
import com.ibs.repository.ProductosRepository;
import com.ibs.resourceOutput.ResponseProducto;
import com.ibs.services.ProductosService;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping(value = "/producto/")
@CrossOrigin
public class ProductosController {

    /** Instancia_Repositorio */
    @Autowired
    ProductosRepository repository;

    @Autowired
    ProductosService service;

    /** Metodo_listar_datos */
    @PostMapping(produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    @Transactional
    public Object getDatos() {
        return service.getDatos();
    }

    /** Metodo_para_editar o guardar_un_registro */
    /** Recibe_entitidad_desde_formulario */
    @PutMapping(produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<Productos> save(@RequestBody Productos entity) {
        return ResponseEntity.ok(repository.save(entity));
    }

    /** Metodo_para buscar_registro por_ID y_cargarlo en_modal_editar */
    @PostMapping(value = "{id}", produces = MediaType.APPLICATION_JSON_VALUE)
    @Transactional
    public ResponseProducto getMethodName(@PathVariable Long id) {

        Productos prod = repository.findById(id).get();

        ResponseProducto a = new ResponseProducto(prod.getId(), prod.getMarca().getMarca(),
                prod.getCategoria().getCategoria(), prod.getCodigo(), prod.getNombre(), prod.getPresentacion(),
                prod.getExistencia(), prod.getPrecioCompra(), prod.getPrecioVenta(), prod.getFechaIngreso(),
                prod.getFechaModificacion());
        return a;
    }

    /** Metodo_para_eliminar_registro */
    @DeleteMapping(value = "{id}", produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public Boolean delete(@PathVariable Long id) {
        Productos entity = repository.findById(id).get();
        repository.delete(entity);
        return true;
    }
}