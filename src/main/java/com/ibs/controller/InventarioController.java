package com.ibs.controller;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;

import javax.transaction.Transactional;

import com.ibs.entities.Detalleinventario;
import com.ibs.entities.Inventarios;
import com.ibs.entities.Inventarios.Evento;
import com.ibs.repository.InventarioRepository;
import com.ibs.repository.ProductoRepository;
import com.ibs.repository.UsuarioRepository;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;

/**
 * RolController
 */
@RestController
@RequestMapping(value = "/inventario/")
@CrossOrigin
public class InventarioController {

	/** Instancia_Repositorio */
	@Autowired
	InventarioRepository rInventario;
	
	@Autowired
	ProductoRepository rProducto;
	
	@Autowired
	UsuarioRepository rUsuario;

	public static List<Detalleinventario> detalles = new ArrayList<Detalleinventario>();

	@GetMapping(value = "agregarDetalle")
	@ResponseBody
	public Detalleinventario agregarDetalle(@RequestParam int cantidad, @RequestParam Long producto) {

		System.out.println("******************************************" + producto);
		final Detalleinventario vp = new Detalleinventario();
		vp.setCantidad(cantidad);
		vp.setProducto(rProducto.findById(producto).get());
		detalles.add(vp);
		return vp;
	}

	@GetMapping(value = "allDetalles", produces = MediaType.APPLICATION_JSON_VALUE)
	@ResponseBody
	@CrossOrigin
	public List<Detalleinventario> getDetallesMemoria() {
		return detalles;
	}

	@GetMapping(value = "detalles", produces = MediaType.APPLICATION_JSON_VALUE)
	@ResponseBody
	public Object getDetalles() {
		return detalles;
	}

	@GetMapping(value = "resetDetalles", produces = MediaType.APPLICATION_JSON_VALUE)
	@ResponseBody
	public Object resetDetalles() {
		detalles = new ArrayList<>();
		// detalles.clear();
		return "lista reseteada";
	}

	/** Metodo_listar */
	@PostMapping(produces = MediaType.APPLICATION_JSON_VALUE)
	@ResponseBody
	@Transactional
	public Object getDatos() {
		String iconoVer = "<i class='fas fa-eye m-1' style='color: black'></i> <strong style='color: black'>Ver</strong>";


		List<HashMap<String, Object>> registros = new ArrayList<>();
		List<Inventarios> lista = (List<Inventarios>) rInventario.findAll();
		for (Inventarios entity : lista) {
			HashMap<String, Object> object = new HashMap<>();

			object.put("id", entity.getId());
			object.put("evento", entity.getEvento());
			object.put("descripcion", entity.getDescripcion());
			object.put("fecha", entity.getFechaIngreso());

			object.put("operaciones",
                    "<button type='button' data-toggle='modal' data-target='#ver' class='btn btn-success ml-3 mt-1'"
							/* + "onclick='cargarRegistro2(" + entity.getId() + ")'> " */ + iconoVer + "</button>");

			registros.add(object);
		}
		return Collections.singletonMap("data", registros);
	}

	@GetMapping(value = "save")
	@ResponseBody
	@CrossOrigin
	public Boolean save(@RequestParam String descripcion, @RequestParam Evento evento, @RequestParam Long idUsuario) {

		Inventarios entity = new Inventarios();

		entity.setEvento(evento);
		entity.setDescripcion(descripcion);
		entity.setUsuario(rUsuario.findById(idUsuario).get());

		for (Detalleinventario detalleInventario : detalles) {
			detalleInventario.setInventarios(entity);
		}

		entity.setDetalleInventario(detalles);

		try {
			rInventario.save(entity);
			return true;
		} catch (Exception e) {
			return false;
		}

	}

}