package com.ibs.controller;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;

import javax.validation.Valid;

import com.ibs.entities.Clientes;
import com.ibs.repository.ClientesRepository;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

/**
 * ClienteController
 */
@RestController
@RequestMapping(value = "/cliente/")
@CrossOrigin
public class ClienteController {

    /** Instancia_Repositorio */
    @Autowired
    ClientesRepository rCliente;

    /** Metodo_listar */
    @PostMapping(produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public Object getDatos() {
        String iconoEditar = "<i class='fas fa-edit m-1' style='color: black'></i> <strong style='color: black'></strong>";
        String iconoEliminar = "<i class='fas fa-trash-alt m-1' style='color: black'></i> <strong style='color: black'></strong>";
        String iconoVer = "<i class='fas fa-eye m-1' style='color: black'></i> <strong style='color: black'></strong>";

        String iconoSeleccionar = "<i class='fa fa-plus m-1'></i> <strong style='color: black'></strong>";

        List<HashMap<String, Object>> registros = new ArrayList<>();
        List<Clientes> lista = (List<Clientes>) rCliente.findAll();

        for (Clientes entity : lista) {
            HashMap<String, Object> object = new HashMap<>();

            object.put("id", entity.getId());
            object.put("username", entity.getUsername());
            object.put("direccion", entity.getDireccion());
            object.put("dui", entity.getDui());
            object.put("giro", entity.getGiro());
            object.put("nit", entity.getNit());
            object.put("nrc", entity.getNrc());
            object.put("telefono", entity.getTelefono());
            object.put("fechaIngreso", entity.getFechaIngreso());
            object.put("fechaModificacion", entity.getFechaModificacion());

            object.put("seleccionar",
                    "<button type='button' data-toggle='modal' data-target='#ver' class='btn btn-primary  ml-3 mt-1'"
                            + "onclick='seleccionarUsuario(" + entity.getId() + ")'> " + iconoSeleccionar + "</button>");
 
            object.put("operacion",
                    "<button type='button' data-toggle='modal' data-target='#ver' class='btn btn-success ml-3 mt-1'"
                            + "onclick='cargarRegistro2(" + entity.getId() + ")'> " + iconoVer + "</button>"
                            + "<button type='button' data-toggle='modal' data-target='#editar' class='btn btn-warning ml-3 mt-1'"
                            + "onclick='cargarRegistro(" + entity.getId() + ")'> " + iconoEditar + "</button>"
                            + "<button type='button' data-toggle='modal' data-target='#eliminar' class='btn btn-danger ml-3 mt-1'"
                            + "onclick='cargarRegistro(" + entity.getId() + ")'> " + iconoEliminar + "</button>");

            registros.add(object);
        }
        return Collections.singletonMap("data", registros);
    }

    /** Metodo_para_editar o guardar_un_registro */
    /** Recibe_entitidad_desde_formulario */
    @PutMapping(produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public ResponseEntity<Clientes> save(@RequestBody @Valid Clientes entity) {
        return ResponseEntity.ok(rCliente.save(entity));
    }

    // Metodo_para buscar_registro por_ID y_cargarlo en_modal_editar
    @PostMapping(value = "{id}", produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public Clientes getMethodName(@PathVariable Long id) {
        return rCliente.findById(id).get();
    }

    // Metodo_para_eliminar_registro
    @DeleteMapping(value = "{id}", produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public Boolean delete(@PathVariable Long id) {
        Clientes entity = rCliente.findById(id).get();
        rCliente.delete(entity);
        return true;
    }

}