package com.ibs.controller;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.ibs.entities.Compras;
import com.ibs.entities.Compras.TipoCompra;
import com.ibs.entities.Detallecompra;
import com.ibs.services.CompraService;

@RestController
@RequestMapping(value = "/compra/")
@CrossOrigin
public class CompraApi {

	@Autowired
	CompraService sCompra;

	public static List<Detallecompra> detalles = new ArrayList<Detallecompra>();

	public static List<Detallecompra> detallesV = new ArrayList<Detallecompra>();

	@GetMapping(value = "agregarDetalle")
	@ResponseBody
	public Detallecompra agregarDetalle(@RequestParam Double precioCompra, @RequestParam int cantidad,
			@RequestParam Long producto) {

		System.out.println("******************************************" + producto);
		final Detallecompra vp = new Detallecompra();
		vp.setCantidad(cantidad);
		vp.setPrecioCompra(precioCompra);
		vp.setProducto(sCompra.getProducto(producto));
		detalles.add(vp);
		return vp;
	}

	@GetMapping(value = "allDetalles", produces = MediaType.APPLICATION_JSON_VALUE)
	@ResponseBody
	@CrossOrigin
	public List<Detallecompra> getDetallesMemoria() {
		return detalles;
	}

	@GetMapping(value = "detalles", produces = MediaType.APPLICATION_JSON_VALUE)
	@ResponseBody
	public Object getDetalles() {
		return detalles;
	}

	@GetMapping(value = "resetDetalles", produces = MediaType.APPLICATION_JSON_VALUE)
	@ResponseBody
	public Object resetDetalles() {
		detalles = new ArrayList<>();
		// detalles.clear();
		return "lista reseteada";
	}

	@GetMapping(value = "save")
	@ResponseBody
	@CrossOrigin
	public Boolean save(@RequestParam String numeroFactura, @RequestParam Long idProveedor,
			@RequestParam Long idUsuario, @RequestParam Double totalCompra) {

		Compras entity = new Compras();

		entity.setProveedor(sCompra.getProveedores(idProveedor));
		entity.setNumeroFactura(numeroFactura);
		entity.setTipoCompra(TipoCompra.Gravadas);
		entity.setUsuario(sCompra.getUsuario(idUsuario));
		entity.setTotalCompra(totalCompra);

		for (Detallecompra detalleVenta : detalles) {
			detalleVenta.setCompras(entity);
		}

		entity.setDetalleCompra(detalles);

		try {
			sCompra.save(entity);
			return true;
		} catch (Exception e) {
			return false;
		}

	}

	@GetMapping(value = "compras", produces = MediaType.APPLICATION_JSON_VALUE)
	@ResponseBody
	@Transactional
	public Object getDatos() {
		String iconoVer = "<i class='fas fa-eye m-1' style='color: black'></i> <strong style='color: black'>Ver</strong>";

		List<HashMap<String, Object>> registros = new ArrayList<>();
		List<Compras> lista = (List<Compras>) sCompra.getAllCompra();

		for (Compras entity : lista) {
			HashMap<String, Object> object = new HashMap<>();

			object.put("id", entity.getId());
			object.put("fechaIngreso", entity.getFechaIngreso());
			object.put("numeroFactura", entity.getNumeroFactura());
			object.put("tipoCompra", entity.getTipoCompra());
			object.put("totalCompra", entity.getTotalCompra());
			object.put("proveedor", entity.getProveedor().getUsername());
			object.put("usuario", entity.getUsuario().getUsername());
			/*
			 * object.put("operacion",
			 * "<button type='button' data-toggle='modal' data-target='#ver' class='btn btn-success ml-3 mt-1'"
			 * + "onclick='cargarRegistro2(" + entity.getId() + ")'> " + iconoVer +
			 * "</button>");
			 */

			registros.add(object);
		}
		return Collections.singletonMap("data", registros);
	}

}