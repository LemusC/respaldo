package com.ibs.controller;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;

import com.ibs.entities.Detalleventa;
import com.ibs.entities.Ventas;
import com.ibs.repository.DetalleVentaRepository;
import com.ibs.repository.VentaRepository;
import com.ibs.entities.Detalleventa.Tipo;
import com.ibs.services.VentaService;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping(value = "/venta/")
@CrossOrigin
public class VentaApi {

	@Autowired
	VentaService sVenta;

	@Autowired
	DetalleVentaRepository rVenta;

	public static List<Detalleventa> detalles = new ArrayList<Detalleventa>();

	@GetMapping(value = "agregarDetalle")
	@ResponseBody
	public Detalleventa agregarDetalle(@RequestParam Tipo tipo, @RequestParam int cantidad,
			@RequestParam Long producto) {

		System.out.println("******************************************" + producto);
		final Detalleventa vp = new Detalleventa();
		vp.setCantidad(cantidad);
		vp.setTipo(tipo);
		vp.setProducto(sVenta.getProducto(producto));
		detalles.add(vp);
		return vp;
	}

	@GetMapping(value = "allDetalles", produces = MediaType.APPLICATION_JSON_VALUE)
	@ResponseBody
	@CrossOrigin
	public List<Detalleventa> getDetallesMemoria() {
		return detalles;
	}

	@GetMapping(value = "detalles", produces = MediaType.APPLICATION_JSON_VALUE)
	@ResponseBody
	public Object getDetalles() {
		return detalles;
	}

	@GetMapping(value = "resetDetalles", produces = MediaType.APPLICATION_JSON_VALUE)
	@ResponseBody
	public Object resetDetalles() {
		detalles = new ArrayList<>();
		// detalles.clear();
		return "lista reseteada";
	}

	@GetMapping(value = "save")
	@ResponseBody
	@CrossOrigin
	public Boolean save(@RequestParam String numeroFactura, @RequestParam Long idCliente, @RequestParam Long idUsuario,
			@RequestParam Double totalVenta, @RequestParam String nombre) {

		Ventas entity = new Ventas();

		entity.setNumeroFactura(numeroFactura);
		entity.setUsuario(sVenta.getUsuario(idUsuario));
		entity.setTotalVenta(totalVenta);

		if (nombre == "" || nombre == null) {
			entity.setCliente(sVenta.getCliente(idCliente));
			entity.setNombre(null);
		} else {
			entity.setNombre(nombre);
			entity.setCliente(null);
		}

		for (Detalleventa detalleVenta : detalles) {
			detalleVenta.setVentas(entity);
		}

		entity.setDetalleVenta(detalles);

		/*
		 * Ventas entity = null;
		 * 
		 * if(nombre == "" || nombre == null && idCliente != null ) { for (Detalleventa
		 * detalleVenta : detalles) { entity = new Ventas(detalles, numeroFactura,
		 * totalVenta, new Clientes( idCliente), new Usuarios( idUsuario));
		 * detalleVenta.setVentas(entity); } }else if(idCliente == null) { for
		 * (Detalleventa detalleVenta : detalles) { entity = new Ventas(nombre,
		 * detalles, numeroFactura, totalVenta, new Usuarios( idUsuario));
		 * detalleVenta.setVentas(entity); } }
		 */

		try {
			sVenta.save(entity);
			return true;
		} catch (Exception e) {
			return false;
		}

	}

	@GetMapping(value = "ventas", produces = MediaType.APPLICATION_JSON_VALUE)
	@ResponseBody
	@Transactional
	public Object getdatos() {
		String iconoVer = "<i class='fas fa-eye m-1' style='color: black'></i> <strong style='color: black'>Ver</strong>";

		final List<HashMap<String, Object>> registros = new ArrayList<>();
		final List<Ventas> lista = (List<Ventas>) sVenta.getAllVenta();

		for (final Ventas entity : lista) {
			final HashMap<String, Object> object = new HashMap<>();

			object.put("id", entity.getId());
			object.put("fecha_venta", entity.getFechaVenta());
			object.put("numero_factura", entity.getNumeroFactura());
			object.put("total_venta", entity.getTotalVenta());

			if (entity.getCliente() == null) {
				object.put("cliente", entity.getNombre());
				// object.put("cliente", "Sin Registro");
			} else {
				object.put("cliente", entity.getCliente().getUsername());
				// object.put("nombre", "no");
			}

			object.put("usuario", entity.getUsuario().getUsername());
			/*
			 * object.put("operaciones",
			 * "<button type='button' data-toggle='modal' data-target='#ver' class='btn btn-success ml-3 mt-1'"
			 * + "onclick='cargarRegistro2(" + entity.getId() + ")'> " + iconoVer +
			 * "</button>");
			 */
			registros.add(object);
		}
		return Collections.singletonMap("data", registros);
	}

	@GetMapping(value = "detalleVenta/{venta_id}", produces = MediaType.APPLICATION_JSON_VALUE)
	@ResponseBody
	@Transactional
	public Object getdatosDetalles(@PathVariable Long id) {

		final List<HashMap<String, Object>> registros = new ArrayList<>();
		final List<Detalleventa> lista = (List<Detalleventa>) rVenta.findByVentasId(id);

		for (final Detalleventa entity : lista) {
			final HashMap<String, Object> object = new HashMap<>();

			object.put("id", entity.getId());
			object.put("cantidad", entity.getCantidad());
			object.put("producto", entity.getProducto());
			object.put("tipo", entity.getTipo());

			registros.add(object);
		}
		return Collections.singletonMap("data", registros);
	}

}