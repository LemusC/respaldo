package com.ibs.entities;

import java.io.Serializable;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import javax.persistence.*;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

import com.fasterxml.jackson.annotation.JsonFormat;

@Entity
@Table(name="inventario")
public class Inventarios implements Serializable {
	private static final long serialVersionUID = 1L;
	
	public enum Evento {
	    Entrada, Salida;
	  }

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name = "id", nullable = false, updatable = false, unique = true)
	private Long id;

	@NotNull
	@Column(nullable = false, length = 30 )
	@NotBlank(message = "descripcion no puede ir vacio")
	private String descripcion;

	@NotNull
	@Enumerated(EnumType.STRING)
	@Column(nullable = false, length = 30 ,columnDefinition = "ENUM('Entrada', 'Salida')")
	private Evento evento;
	
	@Column(nullable = false, columnDefinition="TIMESTAMP DEFAULT CURRENT_TIMESTAMP")
    @JsonFormat(pattern = "yyyy-MM-dd HH-mm")
	private Date fechaIngreso;
	
	@OneToMany(cascade = CascadeType.ALL, fetch = FetchType.LAZY, mappedBy = "inventarios")
	private List<Detalleinventario> detalleInventario;
	
	@NotNull
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "idUsuario", nullable = false)
	private Usuarios usuario;

	public Inventarios() {
	}
	
	public Inventarios(String descripcion, Evento evento) {
	  this.descripcion = descripcion;
	  this.evento = evento;
	}
	
	public Inventarios(Long id, String descripcion, Evento evento) {
	    this.id = id;
	    this.descripcion = descripcion;
	    this.evento = evento;
	}
	
	public String getFechaIngreso() {
		SimpleDateFormat formatoDeFecha = new SimpleDateFormat("dd-MM-yyyy HH:mm");
		String fecha = formatoDeFecha.format(fechaIngreso);
		return fecha;
	}

	public void setFechaIngreso(Date fechaIngreso) {
		this.fechaIngreso = fechaIngreso;
	}

	public Long getId() {
		return this.id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public List<Detalleinventario> getDetalleInventario() {
		return detalleInventario;
	}

	public void setDetalleInventario(List<Detalleinventario> detalleInventario) {
		this.detalleInventario = detalleInventario;
	}

	public String getDescripcion() {
		return this.descripcion;
	}

	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}

	public Evento getEvento() {
		return evento;
	}

	public void setEvento(Evento evento) {
		this.evento = evento;
	}
	
	public Usuarios getUsuario() {
		return usuario;
	}

	public void setUsuario(Usuarios usuario) {
		this.usuario = usuario;
	}
	
}