package com.ibs.entities;

import java.io.Serializable;
import java.text.SimpleDateFormat;

import javax.persistence.*;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.JsonIdentityReference;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.ObjectIdGenerators;

import java.util.Date;

@Entity
@Table(name = "productos")
public class Productos implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "id", nullable = false, updatable = false, unique = true)
	private Long id;

	@NotNull
	@Column(nullable = false, length = 20)
	@NotBlank(message = "codigo no puede ir vacio")
	private String codigo;

	@NotNull
	@Column(nullable = false, length = 25)
	@NotBlank(message = "nombre no puede ir vacio")
	private String nombre;

	@Column(nullable = false, length = 20)
	private int existencia;

	@Column(nullable = false, columnDefinition = "TIMESTAMP DEFAULT CURRENT_TIMESTAMP")
	@JsonFormat(pattern = "yyyy-MM-dd HH-mm")
	private Date fechaIngreso;

	@Column(nullable = false, columnDefinition = "TIMESTAMP DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP")
	@JsonFormat(pattern = "yyyy-MM-dd HH-mm")
	private Date fechaModificacion;

	@NotNull
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "idCategoria", nullable = false)
	@JsonIdentityReference(alwaysAsId = true)
    @JsonIdentityInfo(generator = ObjectIdGenerators.PropertyGenerator.class, property = "id")
    @JsonProperty("idCategoria")
	private Categorias categoria;

	@NotNull
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "idMarca", nullable = false)
	@JsonIdentityReference(alwaysAsId = true)
    @JsonIdentityInfo(generator = ObjectIdGenerators.PropertyGenerator.class, property = "id")
    @JsonProperty("idMarca")
	private Marcas marca;

	@Column(nullable = false, length = 20, precision = 2)
	private double precioCompra;

	@Column(nullable = false, length = 20, precision = 2)
	private double precioVenta;

	@NotNull
	@Column(nullable = false, length = 20)
	@NotBlank(message = "presentacion no puede ir vacio")
	private String presentacion;

	public Productos() {
	}

	public Productos(String codigo, String nombre, Categorias categorias, Marcas marcas, String presentacion) {
		this.codigo = codigo;
		this.nombre = nombre;
		this.categoria = categorias;
		this.marca = marcas;
		this.presentacion = presentacion;
	}


	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getCodigo() {
		return this.codigo;
	}

	public void setCodigo(String codigo) {
		this.codigo = codigo;
	}

	public int getExistencia() {
		return existencia;
	}

	public void setExistencia(int existencia) {
		this.existencia = existencia;
	}

	public Date getFechaIngreso() {
		/*SimpleDateFormat formatoDeFecha = new SimpleDateFormat("dd-MM-yyyy HH:mm");
		String fecha = formatoDeFecha.format(fechaIngreso);*/
		return fechaIngreso;
	}

	public void setFechaIngreso(Date fechaIngreso) {
		this.fechaIngreso = fechaIngreso;
	}

	public Date getFechaModificacion() {
		/*SimpleDateFormat formatoDeFecha = new SimpleDateFormat("dd-MM-yyyy HH:mm");
		String fecha = formatoDeFecha.format(fechaIngreso);*/
		return fechaIngreso;
	}

	public void setFechaModificacion(Date fechaModificacion) {
		this.fechaModificacion = fechaModificacion;
	}

	public Categorias getCategoria() {
		return categoria;
	}

	
	 @JsonProperty("idCategoria") 
	 public void setCategoriaById(Long id) { 
		 categoria= Categorias.fromId(id);
	}

	@JsonIgnore
	public void setCategoria(Categorias categoria) {
		this.categoria = categoria;
	}

	public Marcas getMarca() {
		return marca;
	}

	
	 @JsonProperty("idMarca") 
	 public void setMarcaById(Long id) { 
		 marca = Marcas.fromId(id); 
	}
	 

	@JsonIgnore
	public void setMarca(Marcas marca) {
		this.marca = marca;
	}

	public String getNombre() {
		return this.nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public double getPrecioCompra() {
		return this.precioCompra;
	}

	public void setPrecioCompra(double precioCompra) {
		this.precioCompra = precioCompra;
	}

	public double getPrecioVenta() {
		return this.precioVenta;
	}

	public void setPrecioVenta(double precioVenta) {
		this.precioVenta = precioVenta;
	}

	public String getPresentacion() {
		return this.presentacion;
	}

	public void setPresentacion(String presentacion) {
		this.presentacion = presentacion;
	}

	public static Productos fromId(Long id) {
		Productos entity = new Productos();
		entity.id = id;
		return entity;
	}
	
	

	@Override
	public String toString() {
		return "Productos [id=" + id + ", codigo=" + codigo + ", nombre=" + nombre + ", existencia=" + existencia
				+ ", fechaIngreso=" + fechaIngreso + ", fechaModificacion=" + fechaModificacion + ", categoria="
				+ categoria + ", marca=" + marca + ", precioCompra=" + precioCompra + ", precioVenta=" + precioVenta
				+ ", presentacion=" + presentacion + "]";
	}

	/*
	 * public Double getDescuento() { return this.descuento; }
	 * 
	 * public void setDescuento(Double descuento) { this.descuento = descuento; }
	 */
	

}