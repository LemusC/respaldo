package com.ibs.entities;

import java.io.Serializable;
import java.text.DecimalFormat;
import java.text.SimpleDateFormat;

import javax.persistence.*;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.Date;
import java.util.List;

@Entity
@Table(name="compras")
public class Compras implements Serializable {
	private static final long serialVersionUID = 1L;
	
	public enum TipoCompra {
		Gravadas, Exentas;
	}
	
	transient
	DecimalFormat definirDecimal = new DecimalFormat("0.00");

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name = "id", nullable = false, updatable = false, unique = true)
	private Long id;

	@NotNull
	@Column(name="totalCompra", nullable = false, length = 20  )
	private double totalCompra;

	@Column(nullable = false, columnDefinition="TIMESTAMP DEFAULT CURRENT_TIMESTAMP")
    @JsonFormat(pattern = "yyyy-MM-dd HH-mm")
	private Date fechaIngreso;

	@OneToMany(cascade = CascadeType.ALL, fetch = FetchType.LAZY, mappedBy = "compras")
	private List<Detallecompra> detalleCompra;

	@NotNull
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "idProveedor", nullable = false)
	private Proveedores proveedor;
	
	@NotNull
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "idUsuario", nullable = false)
	private Usuarios usuario;

	@NotNull
	@Column(nullable = false, length = 30 )
	@NotBlank(message = "numeroFactura no puede ir vacio")
	private String numeroFactura;

	@NotNull
	@Enumerated(EnumType.STRING)
	@Column(nullable = false, length = 30 ,columnDefinition = "ENUM('Gravadas', 'Exentas')")
	private TipoCompra tipoCompra;

	public Compras() {
	}

	public Compras(Long id, Double totalCompra, List<Detallecompra> detalleCompra, TipoCompra tipoCompra,
			Proveedores proveedor, String numeroFactura) {
		this.id = id;
		this.numeroFactura = numeroFactura;
		this.tipoCompra = tipoCompra;
		this.totalCompra = totalCompra;
		this.proveedor = proveedor;
		this.detalleCompra = detalleCompra;
	}

	public Compras(Double totalCompra, List<Detallecompra> detalleCompra, TipoCompra tipoCompra,
			Proveedores proveedor, String numeroFactura) {
		this.numeroFactura = numeroFactura;
		this.tipoCompra = tipoCompra;
		this.totalCompra = totalCompra;
		this.proveedor = proveedor;
		this.detalleCompra = detalleCompra;
	}


	public Long getId() {
		return this.id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public double getTotalCompra() {
		/*
		 * String numero = definirDecimal.format(totalCompra); totalCompra =
		 * Double.valueOf(numero);
		 */
		return totalCompra;
	}

	public void setTotalCompra(double totalCompra) {
		this.totalCompra = totalCompra;
	}

	public String getFechaIngreso() {
		SimpleDateFormat formatoDeFecha = new SimpleDateFormat("dd-MM-yyyy HH:mm");
		String fecha = formatoDeFecha.format(fechaIngreso);
		return fecha;
	}

	public void setFechaIngreso(Date fechaIngreso) {
		this.fechaIngreso = fechaIngreso;
	}

	public Proveedores getProveedor() {
		return proveedor;
	}
	
	@JsonProperty("idProveedor")
	public void setClienteById(Long id) {
		proveedor = Proveedores.fromId(id);
	}

	@JsonIgnore
	public void setProveedor(Proveedores proveedor) {
		this.proveedor = proveedor;
	}

	public Usuarios getUsuario() {
		return usuario;
	}

	public void setUsuario(Usuarios usuario) {
		this.usuario = usuario;
	}

	public String getNumeroFactura() {
		return this.numeroFactura;
	}

	public void setNumeroFactura(String numeroFactura) {
		this.numeroFactura = numeroFactura;
	}

	public TipoCompra getTipoCompra() {
		return tipoCompra;
	}

	public void setTipoCompra(TipoCompra tipoCompra) {
		this.tipoCompra = tipoCompra;
	}

	public List<Detallecompra> getDetalleCompra() {
		return this.detalleCompra;
	}

	public void setDetalleCompra(List<Detallecompra> detalleCompra) {
		this.detalleCompra = detalleCompra;
	}
}