package com.ibs.entities;

import java.io.Serializable;
import javax.persistence.*;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.JsonIdentityReference;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.ObjectIdGenerators;

@Entity
@Table(name="inventario")
public class Inventario implements Serializable {
	private static final long serialVersionUID = 1L;
	
	public enum Evento {
	    Entrada, Salida;
	  }

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name = "id", nullable = false, updatable = false, unique = true)
	private Long id;

	@NotNull
	@Column(nullable = false, length = 30 )
	private int cantidad;

	@NotNull
	@Column(nullable = false, length = 30 )
	@NotBlank(message = "descripcion no puede ir vacio")
	private String descripcion;

	@NotNull
	@Enumerated(EnumType.STRING)
	@Column(nullable = false, length = 30 ,columnDefinition = "ENUM('Entrada', 'Salida')")
	private Evento evento;

	//bi-directional many-to-one association to Producto
	@NotNull
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "idProducto", nullable = false)
	@JsonIdentityReference(alwaysAsId = true)
    @JsonIdentityInfo(generator = ObjectIdGenerators.PropertyGenerator.class, property = "id")
    @JsonProperty("idProducto")
	private Productos idProducto;

	public Inventario() {
	}
	
	public Inventario(int cantidad, String descripcion, Evento evento, Productos idProducto) {
	  this.cantidad = cantidad;
	  this.descripcion = descripcion;
	  this.evento = evento;
	  this.idProducto = idProducto;
	}
	
	public Inventario(Long id, int cantidad, String descripcion, Evento evento, Productos idProducto) {
	    this.id = id;
	    this.cantidad = cantidad;
	    this.descripcion = descripcion;
	    this.evento = evento;
	    this.idProducto = idProducto;
	}
	
	public Long getId() {
		return this.id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public int getCantidad() {
		return this.cantidad;
	}

	public void setCantidad(int cantidad) {
		this.cantidad = cantidad;
	}

	public String getDescripcion() {
		return this.descripcion;
	}

	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}

	public Evento getEvento() {
		return evento;
	}

	public void setEvento(Evento evento) {
		this.evento = evento;
	}
	
	@JsonIgnore
	@JoinColumn(name="idProducto", nullable=false)
	public Productos getIdProducto() {
		return idProducto;
	}
	
	@JsonProperty("idProducto")
    public void setById(Long id) {
      idProducto = Productos.fromId(id);
    }
	
	@JsonIgnore
	public void setIdProducto(Productos idProducto) {
		this.idProducto = idProducto;
	}

}