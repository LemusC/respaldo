package com.ibs.entities;

import java.io.Serializable;
import javax.persistence.*;
import javax.validation.constraints.NotNull;

/**
 * The persistent class for the detalleventa database table.
 * 
 */
@Entity
@Table(name = "detalleventa")
public class Detalleventa implements Serializable {
	private static final long serialVersionUID = 1L;

	public enum Tipo {
		EXENTAS, GRAVADAS, NO_SUJETAS;
	}

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "id", nullable = false, updatable = false, unique = true)
	private Long id;

	@NotNull
	@Column(nullable = false, length = 10)
	private int cantidad;

	// bi-directional many-to-one association to Producto
	@NotNull
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "idProducto", nullable = false)
	private Productos producto;

	@NotNull
	@Enumerated(EnumType.STRING)
	@Column(nullable = false, length = 30, columnDefinition = "ENUM('EXENTAS', 'GRAVADAS', 'NO_SUJETAS')")
	private Tipo tipo;

	@JoinColumn(name = "venta_id")
	@ManyToOne(cascade = CascadeType.REMOVE, optional = false, fetch = FetchType.EAGER)
	private Ventas ventas;

	public Detalleventa() {
	}

	public Long getId() {
		return this.id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public int getCantidad() {
		return cantidad;
	}

	public void setCantidad(int cantidad) {
		this.cantidad = cantidad;
	}

	public Ventas getVentas() {
		return ventas;
	}

	public void setVentas(Ventas ventas) {
		this.ventas = ventas;
	}

	public Tipo getTipo() {
		return tipo;
	}

	public void setTipo(Tipo tipo) {
		this.tipo = tipo;
	}

	public Productos getProducto() {
		return this.producto;
	}

	public void setProducto(Productos producto) {
		this.producto = producto;
	}
}