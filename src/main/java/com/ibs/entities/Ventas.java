package com.ibs.entities;

import java.io.Serializable;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import javax.persistence.*;
import javax.validation.constraints.NotNull;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.JsonIdentityReference;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.ObjectIdGenerators;

@Entity
@Table(name = "ventas")
public class Ventas implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "id", nullable = false, updatable = false, unique = true)
	private Long id;

	@Column(length = 30)
	private String nombre;

	@NotNull
	@Column(nullable = false, length = 30)
	private String numeroFactura;

	@Column(name = "totalVenta", nullable = false, length = 20)
	private double totalVenta;

	// bi-directional many-to-one association to Cliente
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "idCliente")
	@JsonIdentityReference(alwaysAsId = true)
	@JsonIdentityInfo(generator = ObjectIdGenerators.PropertyGenerator.class, property = "id")
	@JsonProperty("idCliente")
	private Clientes cliente;

	@OneToMany(cascade = CascadeType.ALL, fetch = FetchType.LAZY, mappedBy = "ventas")
	private List<Detalleventa> detalleVenta;

	@NotNull
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "idUsuario", nullable = false)
	@JsonIdentityReference(alwaysAsId = true)
	@JsonIdentityInfo(generator = ObjectIdGenerators.PropertyGenerator.class, property = "id")
	@JsonProperty("idUsuario")
	private Usuarios usuario;

	@Column(nullable = false, columnDefinition = "TIMESTAMP DEFAULT CURRENT_TIMESTAMP")
	@JsonFormat(pattern = "yyyy-MM-dd")
	private Date fechaVenta;

	public Ventas() {
	}

	public Long getId() {
		return this.id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getNumeroFactura() {
		return this.numeroFactura;
	}

	public void setNumeroFactura(String numeroFactura) {
		this.numeroFactura = numeroFactura;
	}

	public String getNombre() {
		return nombre;
	}

	public double getTotalVenta() {
		return this.totalVenta;
	}

	public void setTotalVenta(double totalVenta) {
		this.totalVenta = totalVenta;
	}

	public Clientes getCliente() {
		return cliente;
	}

	@JsonProperty("idCliente")
	public void setClienteById(Long id) {
		cliente = Clientes.fromId(id);
	}

	@JsonIgnore
	public void setCliente(Clientes cliente) {
		this.cliente = cliente;
	}

	public Usuarios getUsuario() {
		return usuario;
	}

	@JsonProperty("idUsuario")
	public void setUsuarioById(Long id) {
		usuario = Usuarios.fromId(id);
	}

	@JsonIgnore
	public void setUsuario(Usuarios usuario) {
		this.usuario = usuario;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public String getFechaVenta() {
		SimpleDateFormat formatoDeFecha = new SimpleDateFormat("dd-MM-yyyy HH:mm");
		String fecha = formatoDeFecha.format(fechaVenta);
		return fecha;
	}

	public void setFechaVenta(Date fechaVenta) {
		this.fechaVenta = fechaVenta;
	}

	public List<Detalleventa> getDetalleVenta() {
		return this.detalleVenta;
	}

	public void setDetalleVenta(List<Detalleventa> detalleVenta) {
		this.detalleVenta = detalleVenta;
	}

}