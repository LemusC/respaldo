package com.ibs.entities;

import java.io.Serializable;
import javax.persistence.*;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.JsonIdentityReference;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.ObjectIdGenerators;

@Entity
@Table(name = "usuarios", uniqueConstraints = @UniqueConstraint(columnNames = { "correo", "dui" }))
public class Usuarios implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "id", nullable = false, updatable = false, unique = true)
	private Long id;

	@NotNull
	@Column(nullable = false, length = 20)
	@NotBlank(message = "username no puede ir vacio")
	private String username;

	@NotNull
	@Column(nullable = false, length = 20)
	@NotBlank(message = "password no puede ir vacio")
	private String password;

	@NotNull
	@Column(nullable = false, length = 20)
	@NotBlank(message = "correo no puede ir vacio")
	private String correo;

	@NotNull
	@Column(nullable = false, length = 30)
	@NotBlank(message = "direccion no puede ir vacio")
	private String direccion;
 
	@NotNull
	@Column(nullable = false, length = 11)
	@NotBlank(message = "dui no puede ir vacio")
	private String dui;

	@NotNull
	@Column(nullable = false, length = 8)
	private int telefono;

	@NotNull
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "idRol", nullable = false)
	@JsonIdentityReference(alwaysAsId = true)
	@JsonIdentityInfo(generator = ObjectIdGenerators.PropertyGenerator.class, property = "id")
	@JsonProperty("idRol")
	private Role idRol;

	public Usuarios() {
	}

	public Usuarios(Long id, String username, String password, String correo, String direccion, String dui,
			int telefono, Role idRol) {
		this.id = id;
		this.username = username;
		this.password = password;
		this.correo = correo;
		this.direccion = direccion;
		this.dui = dui;
		this.telefono = telefono;
		this.idRol = idRol;
	}

	public Usuarios(String username, String password, String correo, String direccion, String dui, int telefono,
			Role idRol) {
		this.username = username;
		this.password = password;
		this.correo = correo;
		this.direccion = direccion;
		this.dui = dui;
		this.telefono = telefono;
		this.idRol = idRol;
	}

	public Usuarios(Long id) {
		this.id = id;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String getCorreo() {
		return this.correo;
	}

	public void setCorreo(String correo) {
		this.correo = correo;
	}

	public String getDireccion() {
		return this.direccion;
	}

	public void setDireccion(String direccion) {
		this.direccion = direccion;
	}

	public String getDui() {
		return this.dui;
	}

	public void setDui(String dui) {
		this.dui = dui;
	}

	@JsonProperty("idRol")
	public void setById(Long id) {
		idRol = Role.fromId(id);
	}

	public Role getIdRol() {
		return idRol;
	}

	@JsonIgnore
	public void setIdRol(Role idRol) {
		this.idRol = idRol;
	}

	public int getTelefono() {
		return this.telefono;
	}

	public void setTelefono(int telefono) {
		this.telefono = telefono;
	}

	public String getUsername() {
		return this.username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public static Usuarios fromId(Long id) {
		Usuarios entity = new Usuarios();
		entity.id = id;
		return entity;
	}

}