package com.ibs.repository;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import com.ibs.entities.Categorias;

@Repository
public interface CategoriasRepository extends CrudRepository<Categorias, Long> {

}
