package com.ibs.repository;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import com.ibs.entities.Role;

@Repository
public interface RoleRepository extends CrudRepository<Role, Long>{

}
