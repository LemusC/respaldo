package com.ibs.repository;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import com.ibs.entities.Detallecompra;

@Repository
public interface DetalleCompraRepository extends CrudRepository<Detallecompra, Long> {

}

