package com.ibs.repository;

import java.util.List;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import com.ibs.entities.Detalleventa;

@Repository
public interface DetalleVentaRepository extends CrudRepository<Detalleventa, Long> {
	
	List<Detalleventa> findByVentasId(Long id);

}
