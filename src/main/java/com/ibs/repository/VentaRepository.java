package com.ibs.repository;

import com.ibs.entities.Ventas;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

/**
 * VentaRepository
 */
@Repository
public interface VentaRepository extends CrudRepository<Ventas, Long> {

    @Query(value = "SELECT id FROM ventas ORDER BY id DESC LIMIT 1;", nativeQuery = true)
    Long getUltimoIdRegistrado();

}