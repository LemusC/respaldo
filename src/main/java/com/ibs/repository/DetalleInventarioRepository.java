package com.ibs.repository;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import com.ibs.entities.Detalleinventario;

@Repository
public interface DetalleInventarioRepository extends CrudRepository<Detalleinventario, Long> {

}
