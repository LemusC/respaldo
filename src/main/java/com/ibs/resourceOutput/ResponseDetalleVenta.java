package com.ibs.resourceOutput;

import com.ibs.entities.Detalleventa.Tipo;

public class ResponseDetalleVenta {

	Long id;
	int cantidad;
	String producto;
	Tipo tipo;

	public ResponseDetalleVenta(Long id, int cantidad, String producto, Tipo tipo) {
		this.id = id;
		this.cantidad = cantidad;
		this.producto = producto;
		this.tipo = tipo;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public int getCantidad() {
		return cantidad;
	}

	public void setCantidad(int cantidad) {
		this.cantidad = cantidad;
	}

	public String getProducto() {
		return producto;
	}

	public void setProducto(String producto) {
		this.producto = producto;
	}

	public Tipo getTipo() {
		return tipo;
	}

	public void setTipo(Tipo tipo) {
		this.tipo = tipo;
	}

}
