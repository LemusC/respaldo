package com.ibs.resourceOutput;

import java.util.Date;

public class ResponseProducto {
	Long id;
	String marca;
	String categoria;
	String codigo;
	String nombre;
	String presentacion;
	int existencia;
	Double precioCompra;
	Double precioVenta;
	Date fechaIngreso;
	Date fechaModificacion;

	public ResponseProducto(Long id, String marca, String categoria, String codigo, String nombre, String presentacion,
			int existencia, Double precioCompra, Double precioVenta, Date fechaIngreso, Date fechaModificacion) {
		this.id = id;
		this.marca = marca;
		this.categoria = categoria;
		this.codigo = codigo;
		this.nombre = nombre;
		this.presentacion = presentacion;
		this.existencia = existencia;
		this.precioCompra = precioCompra;
		this.precioVenta = precioVenta;
		this.fechaIngreso = fechaIngreso;
		this.fechaModificacion = fechaModificacion;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getMarca() {
		return marca;
	}

	public void setMarca(String marca) {
		this.marca = marca;
	}

	public String getCategoria() {
		return categoria;
	}

	public void setCategoria(String categoria) {
		this.categoria = categoria;
	}

	public String getCodigo() {
		return codigo;
	}

	public void setCodigo(String codigo) {
		this.codigo = codigo;
	}

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public String getPresentacion() {
		return presentacion;
	}

	public void setPresentacion(String presentacion) {
		this.presentacion = presentacion;
	}

	public int getExistencia() {
		return existencia;
	}

	public void setExistencia(int existencia) {
		this.existencia = existencia;
	}

	public Double getPrecioCompra() {
		return precioCompra;
	}

	public void setPrecioCompra(Double precioCompra) {
		this.precioCompra = precioCompra;
	}

	public Double getPrecioVenta() {
		return precioVenta;
	}

	public void setPrecioVenta(Double precioVenta) {
		this.precioVenta = precioVenta;
	}

	public Date getFechaIngreso() {
		return fechaIngreso;
	}

	public void setFechaIngreso(Date fechaIngreso) {
		this.fechaIngreso = fechaIngreso;
	}

	public Date getFechaModificacion() {
		return fechaModificacion;
	}

	public void setFechaModificacion(Date fechaModificacion) {
		this.fechaModificacion = fechaModificacion;
	}

}
