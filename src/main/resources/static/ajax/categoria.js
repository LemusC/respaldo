$(document).ready(inicio);
const api = "/categoria/";

//busca_id
function cargarRegistro(id) {
	$('.btnload').text('Modificar');
	$('.tituloModal').text('Modificar Registro');
	$.ajax({
		url : api + id,
		method : "post",
		success : function(response) {
			$(".focu").focus();
			$("#id").val(response.id);
			$("#categoria").val(response.categoria);
			$("#codigo").val(response.codigo);
			console.log("obtuvo datos " + response.id + response.categoria + response.codigo);
			
			//llenar el modal eliminars
			$("#dato").text(response.categoria);
			setId(response.id);
		},
		error : function(response) {
			alert("Error al obtener el dato");
		}
	});
}

//funcion_listar
function cargarDatos() {
    $("#tablaConsultas").DataTable({

        "ajax": {
            "url": api,
            "method": "Get"
        },
        "columns": [/* {
                "data": "id",
                "width": "5%"
            }, */
            {
                "data": "categoria",
                "width": "20%"
            },
            {
                "data": "codigo",
                "width": "20%"
            },
            {
                "data": "operacion",
                "width": "10%"
            }
        ],
        
        "language": {
            "lengthMenu": "Mostrar _MENU_ ",
            "zeroRecords": "Datos no encontrados",
            "info": "Mostar páginas _PAGE_ de _PAGES_",
            "infoEmpty": "Datos no encontrados",
            "infoFiltered": "(Filtrados por _MAX_ total registros)",
            "search": "Buscar:",
            "paginate": {
                "first": "Primero",
                "last": "Anterior",
                "next": "Siguiente",
                "previous": "Anterior"
            }
        },
        "bDestroy": "true"
    });
}