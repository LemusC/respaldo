$(document).ready(inicio);
const api = "/compra/";

//funcion_listar
function cargarDatos() {
    $("#tablaCompras").DataTable({

        "ajax": {
            "url": api + "compras",
            "method": "Get"
        },
        "columns": [/* {
                "data": "id",
                "width": "5%"
            }, */
            {
                "data": "fechaIngreso",
                
            },
 	    {
                "data": "numeroFactura",
                
            },
            {
                "data": "tipoCompra",
                
            },
            {
                "data": "totalCompra",
                
            },
            {
                "data": "proveedor",
                
            },
            {
                "data": "usuario",
                
            },
            {
                "data": "operacion",
                
            }
        ],
        
        "language": {
            "lengthMenu": "Mostrar _MENU_ ",
            "zeroRecords": "Datos no encontrados",
            "info": "Mostar páginas _PAGE_ de _PAGES_",
            "infoEmpty": "Datos no encontrados",
            "infoFiltered": "(Filtrados por _MAX_ total registros)",
            "search": "Buscar:",
            "paginate": {
                "first": "Primero",
                "last": "Anterior",
                "next": "Siguiente",
                "previous": "Anterior"
            }
        },
        "bDestroy": "true"
    });
}                        