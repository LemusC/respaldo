$(document).ready(inicio);
const api = "/cliente/";

//busca_id
function cargarRegistro(id) {
    $('.btnload').text('Modificar');
    $('.tituloModal').text('Modificar Registro');
    $.ajax({
        url: api + id,
        method: "post",
        success: function (response) {
            $("#id").val(response.id);
            $("#username").val(response.username);
            $("#direccion").val(response.direccion);
            $("#dui").val(response.dui);
            $("#giro").val(response.giro);
            $("#nit").val(response.nit);
            $("#nrc").val(response.nrc);
            $("#telefono").val(response.telefono);

            //llenar el modal eliminars
            $("#dato").text(response.username);
            setId(response.id);
        },
        error: function (response) {
            alert("Error al obtener el dato");
        }
    });
}

function cargarRegistro2(id) {
    $('.tituloModal').text('Cliente');
    $.ajax({
        url: api + id,
        method: "post",
        success: function (response) {
            $("#id2").val(response.id);
            $("#username2").val(response.username);
            $("#direccion2").val(response.direccion);
            $("#dui2").val(response.dui);
            $("#giro2").val(response.giro);
            $("#nit2").val(response.nit);
            $("#nrc2").val(response.nrc);
            $("#telefono2").val(response.telefono);
            $("#fechaIngreso2").val(response.fechaIngreso);
            $("#fechaModificacion2").val(response.fechaModificacion);

            //llenar el modal eliminars
            $("#dato").text(response.username);
            setId(response.id);
        },
        error: function (response) {
            alert("Error al obtener el dato");
        }
    });
}

//funcion_listar
function cargarDatos() {
    $("#tablaClientes").DataTable({

        "ajax": {
            "url": api,
            "method": "post"
        },
        "columns": [
            /* {
                            "data": "id",
                            "width": "5%"
                        }, */
            {
                "data": "username",
                "width": "20%"
            },
            {
                "data": "direccion",
                "width": "20%"
            },
            /* {
            	"data": "departamento",
            	"width": "20%"
            },
            {
            	"data": "dui",
            	"width": "20%"
            }, */
            {
                "data": "giro",
                "width": "20%"
            },
            /* {
            	"data": "nit",
            	"width": "20%"
            }, */
            {
                "data": "nrc",
                "width": "20%"
            },
            {
                "data": "telefono",
                "width": "20%"
            },
            {
                "data": "operacion",
                "width": "5%"
            }
        ],

        "language": {
            "lengthMenu": "Mostrar _MENU_ ",
            "zeroRecords": "Datos no encontrados",
            "info": "Mostar páginas _PAGE_ de _PAGES_",
            "infoEmpty": "Datos no encontrados",
            "infoFiltered": "(Filtrados por _MAX_ total registros)",
            "search": "Buscar:",
            "paginate": {
                "first": "Primero",
                "last": "Anterior",
                "next": "Siguiente",
                "previous": "Anterior"
            }
        },
        "bDestroy": "true"
    });
}