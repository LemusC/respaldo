$(document).ready(inicio);
var apiCliente = "/cliente/";
var apiProductos = "/producto/";
const apiVenta = "/venta/";

let cliente = {
    id: 0,
    nombre: ""
};

let producto = {
    id: 0,
    nombre: ""
};

function inicio() {
	cargarDatos();
	
    $("#guardarVenta").click(guardarVenta);
    resetDetalles();
    
    $("#ver").click(function () {
        $('#modalDetalleVenta').modal('show');
        
    });


    $("#buscarCliente").click(function () {
        $('#modalCliente').modal('show');
        listarClientes();
    });

    $("#buscarProducto").click(function () {
        $('#modalProductos').modal('show');
        listarProductos();
    });

    $("#agregarDetalle").click(agregarDetalle);

}

function seleccionarProducto(id) {
    $.ajax({
        url: apiProductos + "ventas",
        method: "post",
        success: function (response) {
            $("#producto").val(response.nombre);

            producto.id = response.id;
            producto.nombre = response.nombre;
            $('#modalProductos').modal('hide');
        },
        error: function (response) {
            alert("Error al obtener el dato");
        }
    });
}

function agregarDetalle() {
    $.ajax({
        url: "/venta/agregarDetalle",
        method: "get",
        data: {
            cantidad: $("#cantidad").val(),
            producto: producto.id,
            tipo: $("#tipo").val()

        },
        success: function (response) {
            console.log(producto);
            alert("Detalle agregado");
            $("#cantidad").val("");
            $("#producto").val("");
            cargarDetalles();
        },
        error: function (response) {
            alert("Error al obtener el dato");
        }
    });

}

function cargarDatos() {
	$("#tablaventas").DataTable({
		
		"ajax" : {
			"url" : apiVenta + "ventas",
			"method" : "Get"
		},
		"columns" : [/*
						 * { "data": "id", },
						 */
		{
			"data" : "fecha_venta",
		}, /*{
			"data" : "nombre",
		}, */{
			"data" : "numero_factura",
		}, /*{
			"data" : "tipo_venta",
		}, */{
			"data" : "total_venta",
		}, {
			"data" : "cliente",
		}, /*{
			"data" : "usuario",
		}, {
			"data" : "operaciones",
		}, */
		],

		"language" : {
			"lengthMenu" : "Mostrar _MENU_ ",
			"zeroRecords" : "Datos no encontrados",
			"info" : "Mostar páginas _PAGE_ de _PAGES_",
			"infoEmpty" : "Datos no encontrados",
			"infoFiltered" : "(Filtrados por _MAX_ total registros)",
			"search" : "Buscar:",
			"paginate" : {
				"first" : "Primero",
				"last" : "Anterior",
				"next" : "Siguiente",
				"previous" : "Anterior"
			}
		},
		"bDestroy" : "true"
	});
}

//MODAL AGREGAR CLIENTE A LA VENTA
function seleccionarUsuario(id) {
    $.ajax({
        url: apiCliente + id,
        method: "post",
        success: function (response) {
            $("#cliente").val(response.username);
            producto.id = response.id;
            cliente.id = response.id;
            $('#modalCliente').modal('hide');
        },
        error: function (response) {
            alert("Error al obtener el dato");
        }
    });
}

function seleccionarProducto(id) {
    $.ajax({
        url: apiProductos + id,
        method: "post",
        success: function (response) {
            $("#producto").val(response.nombre);

            producto.id = response.id;
            producto.nombre = response.nombre;
            $('#modalProductos').modal('hide');
        },
        error: function (response) {
            alert("Error al obtener el dato");
        }
    });
}

function cargarDetalles() {
    $.ajax({
        url: "/venta/allDetalles",
        method: "Get",
        success: function (response) {
            $("#tDetalles").html("");
            var subTotal = 0,
                subTotalExc = 0,
                subTotalNoSuj = 0,
                subTotalIva = 0,
                total = 0,
                gravadas1 = 0;
            response.forEach(i => {

                var no_sujetas, exentas, gravadas;
                var iva;

                if (i.tipo == "EXENTAS") {
                    //exentas = i.producto.precioVenta * i.cantidad;
                    exentas = (i.producto.precioVenta - (i.producto.precioVenta * 0.13)) * i.cantidad;
                    no_sujetas = 0.00;
                    gravadas = 0.00;

                } else if (i.tipo == "NO_SUJETAS") {
                    //no_sujetas = i.producto.precioCompra * i.cantidad;
                    no_sujetas = (i.producto.precioVenta - (i.producto.precioVenta * 0.13)) * i.cantidad;
                    exentas = 0.00;
                    gravadas = 0.00;

                } else if (i.tipo == "GRAVADAS") {
                    no_sujetas = 0.00;
                    exentas = 0.00;
                    gravadas = i.producto.precioVenta * i.cantidad;

                }
                gravadas1 = gravadas1 + gravadas;
                var iva1 = gravadas * 0.13
                subTotal = subTotal + (gravadas - iva1);
                //subTotal = gravadas- iva1;
                subTotalExc = exentas + subTotalExc;
                subTotalNoSuj = no_sujetas + subTotalNoSuj;

                //por el momento estatico no se cual iva poner
                iva = gravadas1 * 0.13;
                //iva = iva1;

                total = subTotal + subTotalExc + subTotalNoSuj + iva;

                $("#tDetalles").append(
                    "<tr>" +
                    "<td>" + i.cantidad + "</td>" +
                    "<td>" + i.producto.nombre + "</td>" +
                    //"<td>" + i.producto.precioVenta + "</td>" +
                    "<td>" + no_sujetas + "</td>" +
                    "<td>" + exentas + "</td>" +
                    "<td>" + gravadas + "</td>" +

                    "<td><button class='btn btn-danger'>Eliminar</button></td>" +
                    "</tr>"
                );
                $("#Subtotal").val(subTotal);
                $("#exentas").val(subTotalExc);
                $("#no_sujetas").val(subTotalNoSuj);
                $("#iva").val(iva);
                $("#totalVenta").val(total);
            });
        },
        error: function (response) {}
    });

}


function listarClientes() {
    $("#tablaClientes").DataTable({
        "ajax": {
            "url": apiCliente,
            "method": "post"
        },
        "columns": [{
            "data": "username",
            "width": "20%"
        }, {
            "data": "direccion",
            "width": "20%"
        }, {
            "data": "dui",
            "width": "20%"
        }, {
            "data": "nit",
            "width": "20%"
        }, {
            "data": "nrc",
            "width": "20%"
        }, {
            "data": "giro",
            "width": "20%"
        }, {
            "data": "telefono",
            "width": "20%"
        }, {
            "data": "seleccionar",
            "width": "5%"
        }],

        "language": {
            "lengthMenu": "Mostrar _MENU_ ",
            "zeroRecords": "Datos no encontrados",
            "info": "Mostar páginas _PAGE_ de _PAGES_",
            "infoEmpty": "Datos no encontrados",
            "infoFiltered": "(Filtrados por _MAX_ total registros)",
            "search": "Buscar:",
            "paginate": {
                "first": "Primero",
                "last": "Anterior",
                "next": "Siguiente",
                "previous": "Anterior"
            }
        },
        "bDestroy": "true"
    });
}

function listarProductos() {
    $("#tablaProductos").DataTable({
        "ajax": {
            "url": apiProductos,
            "method": "post"
        },
        "columns": [{
            "data": "codigo",
            "width": "5%"
        }, {
            "data": "nombre",
            "width": "20%"
        }, {
            "data": "presentacion",
            "width": "20%"
        }, {
            "data": "marca",
            "width": "20%"
        }, {
            "data": "categoria",
            "width": "20%"
        }, {
            "data": "existencia",
            "width": "20%"
        }, {
            "data": "seleccionar",
            "width": "5%"
        }],

        "language": {
            "lengthMenu": "Mostrar _MENU_ ",
            "zeroRecords": "Datos no encontrados",
            "info": "Mostar páginas _PAGE_ de _PAGES_",
            "infoEmpty": "Datos no encontrados",
            "infoFiltered": "(Filtrados por _MAX_ total registros)",
            "search": "Buscar:",
            "paginate": {
                "first": "Primero",
                "last": "Anterior",
                "next": "Siguiente",
                "previous": "Anterior"
            }
        },
        "bDestroy": "true"
    });
}

function resetDetalles() {
    $.ajax({
        url: "/venta/resetDetalles",
        method: "Get"
    });
}

function guardarVenta() {
    var nf = $("#numeroFactura").val();
    var tv = $("#tipo").val();
    var totalVenta = $("#totalVenta").val();

    $.ajax({
        url: "/venta/save",
        method: "Get",
        data: {
            idUsuario: 1,
            nombre: $("#nombre").val(),
            numeroFactura: nf,
            tipoVenta: tv,
            idCliente: cliente.id,
            totalVenta: totalVenta
        },
        success: function (response) {

            alert("DETALLE GUARDADO CORRECTAMENTE...");
            location.reload(); //para recargar la página
        },
        error: function (response) {
            alert("DETALLE NO GUARDADO ...");
        }
    })
}