$(document).ready(inicio);
var apiProveedor = "/proveedor/";
var apiProductos = "/producto/";
const apiCompras = "/compra/";

let proveedor = {
    id: 0,
    nombre: ""
};

let producto = {
    id: 0,
    nombre: ""
};

function inicio() {
	cargarDatos();
	
	resetDetalles()
	
	$("#ver").click(function () {
        $('#modalDetalleCompra').modal('show');
    });
	
	$("#guardarCompra").click(guardarCompra);
	
    $("#buscarProveedor").click(function () {
        $('#modalProveedor').modal('show');
        listarProveedores();
    });

    $("#buscarProducto").click(function () {
        $('#modalProductos').modal('show');
        listarProductos();
    });
    
    $("#agregarDetalle").click(function () {
        agregarDetalle();
    });

}

//funcion_listar
function cargarDatos() {
  $("#tablaCompras").DataTable({

      "ajax": {
          "url": apiCompras + "compras",
          "method": "Get"
      },
      "columns": [/* {
              "data": "id",
              "width": "5%"
          }, */
          {
              "data": "fechaIngreso",
              
          },
	    {
              "data": "numeroFactura",
              
          },
          {
              "data": "tipoCompra",
              
          },
          {
              "data": "totalCompra",
              
          },
          {
              "data": "proveedor",
              
          },
          {
              "data": "usuario",
              
          },
          {
              "data": "operacion",
              "width": "5%"
          }
      ],
      
      "language": {
          "lengthMenu": "Mostrar _MENU_ ",
          "zeroRecords": "Datos no encontrados",
          "info": "Mostar páginas _PAGE_ de _PAGES_",
          "infoEmpty": "Datos no encontrados",
          "infoFiltered": "(Filtrados por _MAX_ total registros)",
          "search": "Buscar:",
          "paginate": {
              "first": "Primero",
              "last": "Anterior",
              "next": "Siguiente",
              "previous": "Anterior"
          }
      },
      "bDestroy": "true"
  });
}                        

function agregarDetalle() {
    $.ajax({
        url: "/compra/agregarDetalle",
        method: "get",
        data: {
            cantidad: $("#cantidad").val(),
            producto: producto.id,
            precioCompra: $("#precioCompra").val()

        },
        success: function (response) {
            console.log(producto);
            alert("Detalle agregado");
            $("#cantidad").val("");
            $("#producto").val("");
            $("#precioCompra").val("")
            cargarDetalles();
        },
        error: function (response) {
            alert("Error al obtener el dato");
        }
    });

}

// MODAL AGREGAR PROVEEDOR A LA VENTA
function seleccionarUsuario(id) {
    $.ajax({
        url: apiProveedor + id,
        method: "post",
        success: function (response) {
            $("#proveedor").val(response.username);
            producto.id = response.id;
            proveedor.id = response.id;
            $('#modalProveedor').modal('hide');
        },
        error: function (response) {
            alert("Error al obtener el dato");
        }
    });
}

function seleccionarProducto(id) {
    $.ajax({
        url: apiProductos + id,
        method: "post",
        success: function (response) {
            $("#producto").val(response.nombre);
            
            producto.id = response.id;
            producto.nombre = response.nombre;
            $('#modalProductos').modal('hide');
        },
        error: function (response) {
            alert("Error al obtener el dato");
        }
    });
}

function cargarDetalles() {
    $.ajax({
        url: "/compra/allDetalles",
        method: "Get",
        success: function (response) {
            $("#tDetalles").html("");
            
            var subTotalLista = 0,
            	total = 0,
            	subTotal = 0;
            
            response.forEach(i => {
            	
            	subTotalLista = i.cantidad * i.precioCompra;
            	total = subTotalLista + total;
            	
                $("#tDetalles").append("" +
                    "<tr>" +
                    "<td>" + i.cantidad + "</td>" +
                    "<td>" + i.producto.nombre + "</td>" +
                    "<td>$" + i.precioCompra + "</td>" +
                    "<td>$" + subTotalLista + "</td>" +
                    "<td><button class='btn btn-danger'>Eliminar</button></td>" +
                    "</tr>" +
                    "");
                $("#totalCompra").val(total); 
            });
        },
        error: function (response) {}
    });
}

function listarProveedores() {
    $("#tablaProveedores").DataTable({
        "ajax": {
            "url": apiProveedor,
            "method": "post"
        },
        "columns": [{
            "data": "username",
            "width": "20%"
        }, {
            "data": "direccion",
            "width": "20%"
        }, {
            "data": "dui",
            "width": "20%"
        }, {
            "data": "telefono",
            "width": "20%"
        }, {
            "data": "correo",
            "width": "20%"
        }, {
            "data": "seleccionar",
            "width": "5%"
        }],

        "language": {
            "lengthMenu": "Mostrar _MENU_ ",
            "zeroRecords": "Datos no encontrados",
            "info": "Mostar páginas _PAGE_ de _PAGES_",
            "infoEmpty": "Datos no encontrados",
            "infoFiltered": "(Filtrados por _MAX_ total registros)",
            "search": "Buscar:",
            "paginate": {
                "first": "Primero",
                "last": "Anterior",
                "next": "Siguiente",
                "previous": "Anterior"
            }
        },
        "bDestroy": "true"
    });
}

function listarProductos() {
    $("#tablaProductos").DataTable({
        "ajax": {
            "url": apiProductos,
            "method": "post"
        },
        "columns": [{
            "data": "codigo",
            "width": "5%"
        }, {
            "data": "nombre",
            "width": "20%"
        }, {
            "data": "presentacion",
            "width": "20%"
        }, {
            "data": "marca",
            "width": "20%"
        }, {
            "data": "categoria",
            "width": "20%"
        }, {
            "data": "seleccionar",
            "width": "5%"
        }],

        "language": {
            "lengthMenu": "Mostrar _MENU_ ",
            "zeroRecords": "Datos no encontrados",
            "info": "Mostar páginas _PAGE_ de _PAGES_",
            "infoEmpty": "Datos no encontrados",
            "infoFiltered": "(Filtrados por _MAX_ total registros)",
            "search": "Buscar:",
            "paginate": {
                "first": "Primero",
                "last": "Anterior",
                "next": "Siguiente",
                "previous": "Anterior"
            }
        },
        "bDestroy": "true"
    });
}

function resetDetalles() {
    $.ajax({
        url: "/compra/resetDetalles",
        method: "Get"
    });
}

function guardarCompra() {
    var nf = $("#numeroFactura").val();
    // var tv = $("#tipo").val();
    var totalCompra = $("#totalCompra").val();

    $.ajax({
        url: "/compra/save",
        method: "Get",
        data: {
            idUsuario: 1,
            numeroFactura: nf,
            // tipoVenta: tv,
            totalCompra: totalCompra,
            idProveedor: proveedor.id
        },
        success: function (response) {

            alert("DETALLE GUARDADO CORRECTAMENTE...");
            location.reload(); // para recargar la página
        },
        error: function (response) {
            alert("DETALLE NO GUARDADO ...");
        }
    });
}