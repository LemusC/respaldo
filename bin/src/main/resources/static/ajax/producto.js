$(document).ready(inicio);
const api = "/producto/";

//busca_id
function cargarRegistro(id) {
    $('.btnload').text('Modificar');
    $('.tituloModal').text('Modificar Registro');
    $.ajax({
        url: api + id,
        method: "post",
        success: function (response) {
            $(".focu").focus();
            $("#id").val(id);
            $("#codigo").val(response.codigo);
            $("#nombre").val(response.nombre);
            $("#presentacion").val(response.presentacion);

            //llenar el modal eliminar
            $("#dato").text(response.nombre);
            setId(id);
            console.log(model.id + " " + "set id")
        },
        error: function (response) {
            alert("Error al obtener el dato");
        }
    });
}

function cargarRegistro2(id) {
    $('.tituloModal').text('Producto');
    $.ajax({
        url: api + id,
        method: "post",
        success: function (response) {
        	console.log(response);
            $(".focu").focus();
            $("#id2").val(response.id);
            $("#codigo2").val(response.codigo);
            $("#nombre2").val(response.nombre);
            $("#existencia2").val(response.existencia);
            $("#precioCompra2").val("$ " + response.precioCompra);
            $("#precioVenta2").val("$ " + response.precioVenta);
            $("#presentacion2").val(response.presentacion);
            $("#categoria2").val(response.categoria);
            $("#marca2").val(response.marca);
            $("#fechaIngreso2").val(response.fechaIngreso);
            $("#fechaModificacion2").val(response.fechaModificacion);

            //llenar el modal eliminar
            $("#dato").text(response.nombre);
            setId(response.id);
        },
        error: function (response) {
            alert("Error al obtener el dato");
        }
    });
}

//funcion_listar
function cargarDatos() {
    $("#tablaConsultas").DataTable({

        "ajax": {
            "url": api,
            "method": "post"
        },
        "columns": [
            /* {
                            "data": "id",
                            "width": "5%"
                        }, */
            {
                "data": "codigo",
                "width": "5%"
            },
            {
                "data": "nombre",
                "width": "5%"
            },
            {
                "data": "presentacion",
                "width": "5%"
            }, {
                "data": "categoria",
                "width": "5%"
            }, {
                "data": "marca",
                "width": "5%"
            },
            {
                "data": "existencia",
                "width": "5%"
            },
            /* {
                "data": "fechaIngreso",
                "width": "5%"
            },
            {
                "data": "fechaModificacion",
                "width": "5%"
            }, 
            {
                "data": "precioCompra",
                "width": "5%"
            },
            {
                "data": "precioVenta",
                "width": "5%"
            },*/
            {
                "data": "operacion",
                "width": "5%"
            }
        ],

        "language": {
            "lengthMenu": "Mostrar _MENU_ ",
            "zeroRecords": "Datos no encontrados",
            "info": "Mostar páginas _PAGE_ de _PAGES_",
            "infoEmpty": "Datos no encontrados",
            "infoFiltered": "(Filtrados por _MAX_ total registros)",
            "search": "Buscar:",
            "paginate": {
                "first": "Primero",
                "last": "Anterior",
                "next": "Siguiente",
                "previous": "Anterior"
            }
        },
        "bDestroy": "true"
    });
}