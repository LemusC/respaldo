$(document).ready(inicio);
const apiInventario = "/inventario/";
var apiProductos = "/producto/";

let producto = {
	    id: 0,
	    nombre: ""
	};

function inicio() {
	cargarDatos();
	
	$("#ver").click(function () {
        $('#modalDetalleMovimiento').modal('show');
    });
	
	$("#guardarMovimiento").click(guardarMovimiento);
    resetDetalles();
    
    $("#buscarProducto").click(function () {
        $('#modalProductos').modal('show');
        listarProductos();
    });
    
    $("#agregarDetalle").click(agregarDetalle);
}

function seleccionarProducto(id) {
    $.ajax({
        url: apiProductos + id,
        method: "post",
        success: function (response) {
            $("#producto").val(response.nombre);

            producto.id = response.id;
            producto.nombre = response.nombre;
            $('#modalProductos').modal('hide');
        },
        error: function (response) {
            alert("Error al obtener el dato");
        }
    });
}

//busca_id
function cargarRegistro(id) {
	$('.btnload').text('Modificar');
	$('.tituloModal').text('Modificar Registro');
	$.ajax({
		url : api + id,
		method : "post",
		success : function(response) {
			$("#id").val(response.id);
			$("#idProducto").val(response.idProducto);
			$("#evento").val(response.evento);
			$("#cantidad").val(response.cantidad);
			$("#descripcion").val(response.descripcion);
			
			//llenar el modal eliminars
			$("#dato").text(response.idProducto);
			setId(response.id);
		},
		error : function(response) {
			alert("Error al obtener el dato");
		}
	});
}

//funcion_listar
function cargarDatos() {
    $("#tablaConsultas").DataTable({

        "ajax": {
            "url": apiInventario,
            "method": "post"
        },
        "columns": [
            {
                "data": "evento",
                "width": "20%"
            },
            {
            	"data": "descripcion",
            	"width": "20%"
            },
            {
            	"data": "fecha",
            	"width": "20%"
            },
            {
            	"data": "operaciones",
            	"width": "20%"
            },
        ],
        
        "language": {
            "lengthMenu": "Mostrar _MENU_ ",
            "zeroRecords": "Datos no encontrados",
            "info": "Mostar páginas _PAGE_ de _PAGES_",
            "infoEmpty": "Datos no encontrados",
            "infoFiltered": "(Filtrados por _MAX_ total registros)",
            "search": "Buscar:",
            "paginate": {
                "first": "Primero",
                "last": "Anterior",
                "next": "Siguiente",
                "previous": "Anterior"
            }
        },
        "bDestroy": "true"
    });
}

function listarProductos() {
    $("#tablaProductos").DataTable({
        "ajax": {
            "url": apiProductos,
            "method": "post"
        },
        "columns": [{
            "data": "codigo",
            "width": "5%"
        }, {
            "data": "nombre",
            "width": "20%"
        }, {
            "data": "presentacion",
            "width": "20%"
        }, {
            "data": "marca",
            "width": "20%"
        }, {
            "data": "categoria",
            "width": "20%"
        }, {
            "data": "seleccionar",
            "width": "5%"
        }],

        "language": {
            "lengthMenu": "Mostrar _MENU_ ",
            "zeroRecords": "Datos no encontrados",
            "info": "Mostar páginas _PAGE_ de _PAGES_",
            "infoEmpty": "Datos no encontrados",
            "infoFiltered": "(Filtrados por _MAX_ total registros)",
            "search": "Buscar:",
            "paginate": {
                "first": "Primero",
                "last": "Anterior",
                "next": "Siguiente",
                "previous": "Anterior"
            }
        },
        "bDestroy": "true"
    });
}

function agregarDetalle() {
    $.ajax({
        url: apiInventario + "agregarDetalle",
        method: "get",
        data: {
            cantidad: $("#cantidad").val(),
            producto: producto.id
        },
        success: function (response) {
            alert("Detalle agregado");
            $("#cantidad").val("");
            $("#producto").val("");
            cargarDetalles();
        },
        error: function (response) {
            alert("Error al obtener el dato");
        }
    });

}

function resetDetalles() {
    $.ajax({
        url: apiInventario + "resetDetalles",
        method: "Get"
    });
}

function guardarMovimiento() {
   
    $.ajax({
        url: apiInventario + "save",
        method: "Get",
        data: {
        	idUsuario: 1,
        	descripcion: $("#descripcion").val(),
        	evento: $("#evento").val()
        },
        success: function (response) {

            alert("DETALLE GUARDADO CORRECTAMENTE...");
            location.reload(); //para recargar la página
        },
        error: function (response) {
            alert("DETALLE NO GUARDADO ...");
        }
    })
}

function cargarDetalles() {
    $.ajax({
        url: apiInventario + "allDetalles",
        method: "Get",
        success: function (response) {
            $("#tDetalles").html("");
            
            response.forEach(i => {
            	
                $("#tDetalles").append("" +
                    "<tr>" +
                    "<td>" + i.cantidad + "</td>" +
                    "<td>" + i.producto.nombre + "</td>" +
                    "<td><button class='btn btn-danger'>Eliminar</button></td>" +
                    "</tr>" +
                    "");
            });
        },
        error: function (response) {}
    });
}
